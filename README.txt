
Module: Bablic
Author: Ishai Jaffe <http://drupal.org/user/2388048>


Description
===========
Adds the Bablic snippet to your website.

Requirements
============

* Bablic account


Installation
============
* Copy the 'bablic' module directory in to your Drupal
sites/all/modules directory as usual.


Usage
=====
In the settings page enter your Bablic ID.

All pages will now have the required JavaScript added to the
HTML footer can confirm this by viewing the page source from
your browser.

Documentation
=============
You can get more information about the service at:
http://www.bablic.com