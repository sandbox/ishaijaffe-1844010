<?php

/**
 * @file
 * Definition of variables for Variable API module.
 */

/**
 * Implements hook_variable_info().
 */
function bablic_variable_info($options) {
  $variables['bablic_account'] = array(
    'type' => 'string',
    'title' => t('Bablic ID', array(), $options),
    'default' => 'UA-',
    'description' => t('This ID is unique to each site you want to localize, and is a 24 long hexa string. To get a Bablic ID, <a href="@bablic">register your site with Bablic</a>, or if you already have registered your site, go to your Bablic console page to see the ID in the page url path on the address bar. <a href="@bablicid">Find more information in the documentation</a>.', array('@bablic' => 'http://www.bablic.com', '@bablicid' => url('http://www.bablic.com/docs#gettingid'))),
    'required' => TRUE,
    'group' => 'bablic',
    'localize' => TRUE,
    'validate callback' => 'bablic_validate_bablic_account',
  );

  return $variables;
}

/**
 * Implements hook_variable_group_info().
 */
function bablic_variable_group_info() {
  $groups['bablic'] = array(
    'title' => t('Bablic'),
    'description' => t('Configure your Bablic account, to have your site localized into multiple languages.'),
    'access' => 'administer bablic',
    'path' => array('admin/config/system/bablic'),
  );

  return $groups;
}

/**
 * Validate Web Property ID variable.
 */
function bablic_validate_bablic_account($variable) {
  // Replace all type of dashes (n-dash, m-dash, minus) with the normal dashes.
  $variable['value'] = str_replace(array('–', '—', '−'), '-', $variable['value']);

  if (!preg_match('/^[0-9a-f]{24}$/', $variable['value'])) {
    return t('A valid Bablic ID is case sensitive 24 long hexa string.');
  }
}
