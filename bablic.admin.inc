<?php

/**
 * @file
 * Administrative page callbacks for the bablic module.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function bablic_admin_settings_form($form_state) {
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );

  $form['account']['bablic_account'] = array(
    '#title' => t('Bablic ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('bablic_account', ''),
    '#size' => 15,
    '#maxlength' => 24,
    '#required' => TRUE,
    '#description' => t('This ID is unique to each site you want to localize, and is a 24 long hexa string. To get a Bablic ID, <a href="@bablic">register your site with Bablic</a>, or if you already have registered your site, go to your Bablic console page to see the ID in the page url path on the address bar. <a href="@bablicid">Find more information in the documentation</a>.', array('@bablic' => 'http://www.bablic.com', '@bablicid' => url('http://www.bablic.com/docs#gettingid'))),
  );

  // Visibility settings.
  $form['conf_title'] = array(
    '#type' => 'item',
    '#title' => t('Website Localization'),
  );
  $form['conf'] = array(
    '#type' => 'vertical_tabs',
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'bablic') . '/bablic.admin.js'),
    ),
  );

  $form['conf']['domain_tracking'] = array(
    '#type' => 'fieldset',
    '#title' => t('Domains'),
  );

  global $cookie_domain;
  $multiple_sub_domains = array();
  foreach (array('www', 'app', 'shop') as $subdomain) {
    if (count(explode('.', $cookie_domain)) > 2 && !is_numeric(str_replace('.', '', $cookie_domain))) {
      $multiple_sub_domains[] = $subdomain . $cookie_domain;
    }
    // IP addresses or localhost.
    else {
      $multiple_sub_domains[] = $subdomain . '.example.com';
    }
  }

  $multiple_toplevel_domains = array();
  foreach (array('.com', '.net', '.org') as $tldomain) {
    $host = $_SERVER['HTTP_HOST'];
    $domain = substr($host, 0, strrpos($host, '.'));
    if (count(explode('.', $host)) > 2 && !is_numeric(str_replace('.', '', $host))) {
      $multiple_toplevel_domains[] = $domain . $tldomain;
    }
    // IP addresses or localhost
    else {
      $multiple_toplevel_domains[] = 'www.example' . $tldomain;
    }
  }

  $form['conf']['domain_tracking']['bablic_domain_mode'] = array(
    '#type' => 'radios',
    '#title' => t('What domain to add Bablic?'),
    '#options' => array(
      0 => t('A single domain (default)') . '<div class="description">' . t('Domain: @domain', array('@domain' => $_SERVER['HTTP_HOST'])) . '</div>',
      1 => t('One domain with multiple subdomains') . '<div class="description">' . t('Examples: @domains', array('@domains' => implode(', ', $multiple_sub_domains))) . '</div>',
      2 => t('Multiple top-level domains') . '<div class="description">' . t('Examples: @domains', array('@domains' => implode(', ', $multiple_toplevel_domains))) . '</div>',
    ),
    '#default_value' => variable_get('bablic_domain_mode', 0),
  );
  $form['conf']['domain_tracking']['bablic_cross_domains'] = array(
    '#title' => t('List of top-level domains'),
    '#type' => 'textarea',
    '#default_value' => variable_get('bablic_cross_domains', ''),
    '#description' => t('If you selected "Multiple top-level domains" above, enter all related top-level domains. Add one domain per line. By default, the data in your reports only includes the path and name of the page, and not the domain name. For more information see section <em>Show separate domain names</em> in <a href="@url">Tracking Multiple Domains</a>.', array('@url' => url('http://support.google.com/analytics/bin/answer.py', array('query' => array('answer' => '1034342'))))),
  );

  // Page specific visibility configurations.
  $visibility = variable_get('bablic_visibility_pages', 0);
  $pages = variable_get('bablic_pages', BABLIC_PAGES);

  $form['conf']['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  if ($visibility == 2) {
    $form['conf']['page_vis_settings'] = array();
    $form['conf']['page_vis_settings']['visibility'] = array('#type' => 'value', '#value' => 2);
    $form['conf']['page_vis_settings']['pages'] = array('#type' => 'value', '#value' => $pages);
  }
  else {
    $options = array(
      t('Every page except the listed pages'),
      t('The listed pages only'),
    );
    $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));


    $title = t('Pages');
	
    $form['conf']['page_vis_settings']['bablic_visibility_pages'] = array(
      '#type' => 'radios',
      '#title' => t('Add Bablic to specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['conf']['page_vis_settings']['bablic_pages'] = array(
      '#type' => 'textarea',
      '#title' => $title,
      '#title_display' => 'invisible',
      '#default_value' => $pages,
      '#description' => $description,
      '#rows' => 10,
    );
  }

  // Render the role overview.
  $form['conf']['role_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
  );

  $form['conf']['role_vis_settings']['bablic_visibility_roles'] = array(
    '#type' => 'radios',
    '#title' => t('Add Bablic for specific roles'),
    '#options' => array(
      t('Add to the selected roles only'),
      t('Add to every role except the selected ones'),
    ),
    '#default_value' => variable_get('bablic_visibility_roles', 1),
  );

  $role_options = array_map('check_plain', user_roles());
  $form['conf']['role_vis_settings']['bablic_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#default_value' => variable_get('bablic_roles', array(1,1,0)),
    '#options' => $role_options,
    '#description' => t('If none of the roles are selected, all users will see translation. If a user has any of the roles checked, that user will have translation (or excluded, depending on the setting above).'),
  );


  // Advanced feature configurations.
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['advanced']['codesnippet'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom JavaScript code'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('You can add custom Bablic <a href="@snippets">code snippets</a> here. These will be added every time tracking is in effect. Before you add your custom code, you should read the <a href="@ga_concepts_overview">Bablic Tracking Code - Functional Overview</a> and the <a href="@ga_js_api">Bablic Tracking API</a> documentation. <strong>Do not include the &lt;script&gt; tags</strong>, and always end your code with a semicolon (;).', array('@snippets' => 'http://drupal.org/node/248699', '@ga_concepts_overview' => 'https://developers.google.com/analytics/resources/concepts/gaConceptsTrackingOverview', '@ga_js_api' => 'https://developers.google.com/analytics/devguides/collection/gajs/methods/')),
  );
  $form['advanced']['codesnippet']['bablic_codesnippet_before'] = array(
    '#type' => 'textarea',
    '#title' => t('Code snippet (before)'),
    '#default_value' => variable_get('bablic_codesnippet_before', ''),
    '#rows' => 5,
    '#description' => t("Code in this textarea will be added <strong>before</strong> _gaq.push(['_trackPageview'])."),
  );
  $form['advanced']['codesnippet']['bablic_codesnippet_after'] = array(
    '#type' => 'textarea',
    '#title' => t('Code snippet (after)'),
    '#default_value' => variable_get('bablic_codesnippet_after', ''),
    '#rows' => 5,
    '#description' => t("Code in this textarea will be added <strong>after</strong> _gaq.push(['_trackPageview']). This is useful if you'd like to track a site in two accounts."),
  );

  return system_settings_form($form);
}

/**
 * Implements _form_validate().
 */
function bablic_admin_settings_form_validate($form, &$form_state) {

  // Trim some text values.
  $form_state['values']['bablic_account'] = trim($form_state['values']['bablic_account']);
  $form_state['values']['bablic_pages'] = trim($form_state['values']['bablic_pages']);
  $form_state['values']['bablic_cross_domains'] = trim($form_state['values']['bablic_cross_domains']);
  $form_state['values']['bablic_codesnippet_before'] = trim($form_state['values']['bablic_codesnippet_before']);
  $form_state['values']['bablic_codesnippet_after'] = trim($form_state['values']['bablic_codesnippet_after']);

  // Replace all type of dashes (n-dash, m-dash, minus) with the normal dashes.
  $form_state['values']['bablic_account'] = str_replace(array('–', '—', '−'), '-', $form_state['values']['bablic_account']);

  if (!preg_match('/^[0-9a-f]{24}$/', $form_state['values']['bablic_account'])) {
    form_set_error('bablic_account', t('A valid Bablic ID is case sensitive 24 length hexa string.'));
  }

  // If multiple top-level domains has been selected, a domain names list is required.
  if ($form_state['values']['bablic_domain_mode'] == 2 && empty($form_state['values']['bablic_cross_domains'])) {
    form_set_error('bablic_cross_domains', t('A list of top-level domains is required if <em>Multiple top-level domains</em> has been selected.'));
  }

  // This is for the Newbie's who cannot read a text area description.
  if (stristr($form_state['values']['bablic_codesnippet_before'], 'google-analytics.com/ga.js')) {
    form_set_error('bablic_codesnippet_before', t('Do not add the tracker code provided by Google into the javascript code snippets! This module already builds the tracker code based on your Bablic account number and settings.'));
  }
  if (stristr($form_state['values']['bablic_codesnippet_after'], 'google-analytics.com/ga.js')) {
    form_set_error('bablic_codesnippet_after', t('Do not add the tracker code provided by Google into the javascript code snippets! This module already builds the tracker code based on your Bablic account number and settings.'));
  }
  if (preg_match('/(.*)<\/?script(.*)>(.*)/i', $form_state['values']['bablic_codesnippet_before'])) {
    form_set_error('bablic_codesnippet_before', t('Do not include the &lt;script&gt; tags in the javascript code snippets.'));
  }
  if (preg_match('/(.*)<\/?script(.*)>(.*)/i', $form_state['values']['bablic_codesnippet_after'])) {
    form_set_error('bablic_codesnippet_after', t('Do not include the &lt;script&gt; tags in the javascript code snippets.'));
  }

  // Header section must be forced for multiple top-level domains.
  if ($form_state['values']['bablic_domain_mode'] == 2) {
    $form_state['values']['bablic_js_scope'] = 'header';
  }
}

